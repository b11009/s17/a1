/*
3. Create an addStudent() function that will accept a name of the student and add it to the student array.

4. Create a countStudents() function that will print the total number of students in the array.

5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive. */
let studentNames = [];

function addStudent(studentName){
	 studentNames.push(studentName);
	 console.log(studentName + " was added to the student's list")
} 

function countStudent(){
	const numberOfStudents = studentNames.reduce(function(x, y){
	return x + 1;
},0);
	 console.log("There are a total of " + numberOfStudents + " students enrolled.")
} 
// Option 2
/*function countStudent(){
	const numberOfStudents = studentNames.length 
	console.log("There are a total of " + numberOfStudents + " students enrolled.")
} */

function printStudent(){
	studentNames.forEach(function(studentName){
	console.log(studentName);
})
}

function findStudent(studentName){
	// console.log(studentNames);
	let searchStudentNameToLowerCase = studentNames.map(function(studentName){
		return studentName.toLowerCase()
	})
	let searchStudentName = searchStudentNameToLowerCase.indexOf(studentName.toLowerCase());
	if (searchStudentName !== -1){
		const sameNames = studentNames.filter(function(name){
			return name.toLowerCase() == studentName.toLowerCase()
			})
		const ISorARE = sameNames.length > 1 ? 'are' : 'is';
		console.log(sameNames + ' ' + ISorARE +' an enrollee' );
	} else {
		console.log('No student found with the name ' + studentName);
	}
}


